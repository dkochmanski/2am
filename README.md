# 2am

Small testing framework for Common Lisp.

## Synopsis

The entire API consists of: `test`, `is`, `signals`, `finishes`,
`run`, `suite` and `(setf suite)`.

```common-lisp
(defpackage :example (:use :cl :2am))
(in-package :example)

(test foo-test
  (is (= 1 1))
  (is (zerop 0)))

(test bar-test
  (signals simple-error
    (error "bar!")))

(suite 'first)

(test first.quux-test.1
  (is t "bah")
  (signals error (error "bah"))
  (is t "booya ~A should be ~A" 4 8))

(test first.quux-test.2
  (is t "bah")
  (signals warning (error "bah"))
  (is nil "booya ~A should be ~A" 4 8))

(suite 'second '(third fourth))

(test second.quux-test
  (is t "bah")
  (signals warning (error "bah"))
  (is nil "booya ~A should be ~A" 4 8))

(suite 'third)

(test third.quux-test.1
  (is t "booya ~A should be ~A" 4 8))

(test third.quux-test.2
  (is t "bah")
  (signals warning (error "bah"))
  (is nil "booya ~A should be ~A" 4 8)
  (is nil "booya ~A should be ~A" 1 28))

(suite 'fourth)

(test fourth.quux-test
  (error "bu")
  (is t "bah")
  (signals warning (error "bah"))
  (error "bah")
  (is nil "booya ~A should be ~A" 4 8))
```

Running:

```common-lisp
CL-USER> (in-package example)
#<"EXAMPLE" package>
EXAMPLE> (run) ; run all tests in default suite
Running test FOO-TEST ..
Running test BAR-TEST .
Did 2 tests (0 crashed), 3 checks.
   Pass: 3 (100%)
   Fail: 0 ( 0%)
; No value

EXAMPLE> (run 'second) ; run suite SECOND and all its sub-suites

--- Running test suite SECOND
Running test SECOND.QUUX-TEST .ff

--- Running test suite FOURTH
Running test FOURTH.QUUX-TEST X

--- Running test suite THIRD
Running test THIRD.QUUX-TEST.1 .
Running test THIRD.QUUX-TEST.2 .fff
Did 4 tests (1 crashed), 8 checks.
   Pass: 3 (38%)
   Fail: 5 (62%)

Failure details:
--------------------------------
 FOURTH.QUUX-TEST:
   CRASH [SIMPLE-ERROR]: bu
--------------------------------
 THIRD.QUUX-TEST.2:
   FAIL: Expected to signal WARNING, but got SIMPLE-ERROR:
bah
   FAIL: booya 4 should be 8
   FAIL: booya 1 should be 28
--------------------------------
 SECOND.QUUX-TEST:
   FAIL: Expected to signal WARNING, but got SIMPLE-ERROR:
bah
   FAIL: booya 4 should be 8
--------------------------------
; No value

EXAMPLE> (foo-test) ; tests are just functions
Running test FOO-TEST ..
Test FOO-TEST: 2 checks.
   Pass: 2 (100%)
   Fail: 0 ( 0%)
; No value

EXAMPLE> (run '(bar-test first.quux-test.2)) ; run particular tests
Running test FIRST.QUUX-TEST.2 .ff
Running test BAR-TEST .
Did 2 tests (0 crashed), 4 checks.
   Pass: 2 (50%)
   Fail: 2 (50%)

Failure details:
--------------------------------
 FIRST.QUUX-TEST.2:
   FAIL: Expected to signal WARNING, but got SIMPLE-ERROR:
bah
   FAIL: booya 4 should be 8
--------------------------------
; No value

EXAMPLE> (setf (suite nil) '(foo-test)) ; set default tests to run
(FOO-TEST)

EXAMPLE> (run)
Running test FOO-TEST ..
Did 1 test (0 crashed), 2 checks.
   Pass: 2 (100%)
   Fail: 0 ( 0%)
; No value
```

## Overview

2am is small, has no dependencies, and will be stable for the
foreseeable future. It was originally written for large multi-threaded
test suites where the complexity of popular testing frameworks caused
problems. 2am is based on 1am with some features wanted for CI and
hierarchical tests.

* The order of tests is shuffled on each run so that bugs concealed by
  unintentional dependencies between tests may be exposed.

* Checks may occur inside threads.

* Tests are runnable as ordinary functions of the same name.

* Tests are compiled up front (some testing frameworks, such as 5am,
  defer compilation by default).

* Type inferencing (if present) works inside the `is` macro.

* Crashes from the individual test called as a function won't be
  handled (user is thrown into the debugger).

* Since 2am is small and stable, copying it into the project being
  tested is a viable option (be sure to use a different package name).

The following features are distinct from 1am:

* If a `serious-condition` is signalled outside the `signals` macro,
  test is considered to be crashed (`simple-warning` won't cause a
  crash since it's not a `serious-condition`). Non-matching signals or
  lack of them in `signals` macro are considered to be failures not
  crashes.

* New test macro `finishes` is present due to non-interactive nature
  of the framework.

* Tests are grouped in a hierarchical suites denoted by symbols
  (default suite is `NIL`). When a test is compiled it is added to the
  current active suite.

* Assertion forms return a value. If check passes, then the form will
  return `T`, otherwise `NIL`.

## API

* [macro] **`test`** `name` *`&body`* `body` -- Define a test function
  and add it to `*tests*`.

* [macro] **`is`** `form` *`&rest`* `reason-args` -- Assert that
  `form` evaluates to non-nil.

* [macro] **`signals`** `condition` *`&body`* `body` -- Assert that
  `body` signals a condition of type `condition`.

* [macro] **`finishes`** *`&body`* `body` -- Assert that `body`
  doesn't signal a condition.

* [function] **`run`** *`&optional`* `tests` -- Run each test in the
  sequence `tests`. Default are `NIL` suite tests. `tests` is a
  symbol, then the corresponding suite with it's sub-suites tests are
  run.

* [function] **`suite`** *`&optional`* `name` `sub` -- Set the active
  suite to `name` and return it's name. Default is the current active
  suite. If `sub` parameter supplied sub-suites are set to the list of
  suites `sub`.

* [function] **`(setf suite)`** `(values tests suites)` `name` -- set
  `name` suite tests to `tests` and its sub-suites to `suites`.

```common-lisp
    (setf (suites 'my-suite)
          (values '(test-foo test-bar) '(my-suite/part1 my-suite/part2)))
```
